#include "SSD1306.h"  //Library for OLED

//Hardware configuration for OLED
#define OLED_ADDRESS 0x3c
#define OLED_SDA 4 // GPIO4
#define OLED_SCL 15 // GPIO15
#define OLED_RST 16 // GPIO16

#define LED 2 //GPIO2
#define BUTTON 0 //GPIO0

SSD1306 display(OLED_ADDRESS, OLED_SDA, OLED_SCL);

void setup() {
  Serial.begin(115200);
  Serial.println();
  Serial.println();

  //OLED Initialization
  pinMode(OLED_RST,OUTPUT);
	digitalWrite(OLED_RST, LOW); // low to reset OLED
	delay(50); 
	digitalWrite(OLED_RST, HIGH); // must be high to turn on OLED
  display.init();
  //display.flipScreenVertically();
  display.setFont(ArialMT_Plain_16);

  //LED Initialization
  pinMode(LED, OUTPUT);
  digitalWrite(LED, HIGH);

  //Button Initialization
  pinMode(BUTTON, INPUT);
}

int off = 0; //for OLED state control

void btn_event()
{
  Serial.println("Botao pressionado!!!!");

  if (off == 0) {
    display.displayOff();
    off = 1;
  }
  
  else
  {
    display.displayOn();
    off = 0;
  }
  
}

int prox_milis = 1000;
void blink_led()
{
  
  if (millis() > prox_milis) {
    digitalWrite(LED, !digitalRead(LED));
    prox_milis = millis() + 100;
  };
  
}

int btn_anterior;
int btn_actual;

void loop() {
  display.clear();
  display.setFont(ArialMT_Plain_10);
  //display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.drawString(15, 0, "ZuKa Technologies :)");

  display.setFont(ArialMT_Plain_24);
  //display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.drawString(30,30, String(millis()));
  display.display();

  int val = digitalRead(BUTTON);
  btn_anterior = btn_actual;
  btn_actual = val;

  //Detetar a transição de 0 para 1 depois de 250ms de funcionamento
  if (btn_actual == 1 and btn_anterior == 0 and millis() > 250) {
    btn_event();
  }

  blink_led();
  delay(10);
}
